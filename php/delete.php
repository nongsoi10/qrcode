<?php
session_start();

header('Content-Type: application/json');

date_default_timezone_set('Asia/Bangkok');
include "../db/db.php";

$id = mysqli_real_escape_string($connection, $_POST['id']);

$query = "
          DELETE FROM `jobmeter` WHERE `jobmeter`.`id` = '$id'";

if ($connection->query($query) === true) {
    echo json_encode(array('status' => '1', 'message' => ' สำเร็จ'));

} else {
    echo json_encode(array('status' => '2', 'message' => 'โปรดต่อติด Admin'));

}

$connection->close();

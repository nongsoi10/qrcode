<?php
## Database connectionfiguration
session_start();

header('Content-Type: application/json');

include "../db/db.php";

$empcode = $_SESSION['textEmp'];
$dateok = date("Y-m-d");

$sql2 = "SELECT officepea FROM `users` WHERE `empcode` = '$empcode'";
$result2 = mysqli_query($connection, $sql2);
if (mysqli_num_rows($result2) > 0) {
    // output data of each row
    while ($row = mysqli_fetch_assoc($result2)) {
        $officepea = $row["officepea"];
    }

} else {
    echo json_encode(array('status' => '3', 'message' => 'Record add successfully'));

    exit;

}

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = $_POST['search']['value']; // Search value

## Search
$searchQuery = " ";
if ($searchValue != '') {
    $searchQuery = " and (workorder like '%" . $searchValue . "%' or
        namecustomer like '%" . $searchValue . "%' or
        address like '%" . $searchValue . "%' or
        pea_new like '%" . $searchValue . "%' or
        pea_old like '%" . $searchValue . "%' or
        tel like'%" . $searchValue . "%'  ) ";
}

## Total number of records without filtering
$sel = mysqli_query($connection, "select count(*) as allcount from jobmeter WHERE `officepea` = '$officepea'");
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of record with filtering
$sel = mysqli_query($connection, "select count(*) as allcount from jobmeter WHERE `officepea` = '$officepea'  " . $searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "select * from jobmeter WHERE `officepea` = '$officepea' " . $searchQuery . " order by " . $columnName . " " . $columnSortOrder . " limit " . $row . "," . $rowperpage;
$empRecords = mysqli_query($connection, $empQuery);
$data = array();

while ($row = mysqli_fetch_assoc($empRecords)) {
    $data[] = array(
        "id" => $row['id'],
        "workorder" => $row['workorder'],
        "namecustomer" => $row['namecustomer'],
        "address" => $row['address'],
        "tel" => $row['tel'],
        "pea_new" => $row['pea_new'],
        "pea_old" => $row['pea_old'],
        "lat" => $row['lat'],
        "lon" => $row['lon'],
        "officepea" => $row['officepea'],
        "edit" => '<button type="button" name="update" id="btEdit" value="' . $row['id'] . '" class="btn btn-warning btn-xs update" >Update</button>',
        "del" => '<button type="button" name="delete" id="btDel" value="' . $row['id'] . '" class="btn btn-danger btn-xs delete" >Delete</button>',
    );
}

## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data,
);

echo json_encode($response);

<?php
session_start();

header('Content-Type: application/json');

date_default_timezone_set('Asia/Bangkok');
include "../db/db.php";

$idup = mysqli_real_escape_string($connection, $_POST['idup']);
$workorder = mysqli_real_escape_string($connection, $_POST['workorder']);
$namecustomer = mysqli_real_escape_string($connection, $_POST['namecustomer']);
$address = mysqli_real_escape_string($connection, $_POST['address']);
$tel = mysqli_real_escape_string($connection, $_POST['tel']);
$pea_new = mysqli_real_escape_string($connection, $_POST['pea_new']);
$pea_old = mysqli_real_escape_string($connection, $_POST['pea_old']);
$lat = mysqli_real_escape_string($connection, $_POST['lat']);
$lon = mysqli_real_escape_string($connection, $_POST['lon']);

$query = "
           UPDATE jobmeter
           SET workorder='$workorder',
           namecustomer='$namecustomer',
           address='$address',
           tel='$tel',
           pea_new = '$pea_new',
           pea_old = '$pea_old',
           lat = '$lat',
           lon = '$lon'
           WHERE id='$idup'";

if ($connection->query($query) === true) {
    echo json_encode(array('status' => '1', 'message' => ' สำเร็จ'));

} else {
    echo json_encode(array('status' => '2', 'message' => 'โปรดต่อติด Admin'));

}

$connection->close();

<?php
session_start();

header('Content-Type: application/json');

date_default_timezone_set('Asia/Bangkok');
include "../db/db.php";

$textWorkorder = mysqli_real_escape_string($connection, $_POST['textWorkorder']);
$textNamecustomer = mysqli_real_escape_string($connection, $_POST['textNamecustomer']);
$textAddress = mysqli_real_escape_string($connection, $_POST['textAddress']);
$textTel = mysqli_real_escape_string($connection, $_POST['textTel']);
$textPeanew = mysqli_real_escape_string($connection, $_POST['textPeanew']);
$textPeaold = mysqli_real_escape_string($connection, $_POST['textPeaold']);
$textLat = mysqli_real_escape_string($connection, $_POST['textLat']);
$textLon = mysqli_real_escape_string($connection, $_POST['textLon']);


$empcode = $_SESSION['textEmp'];
$dateok = date("Y-m-d");

$sql2 = "SELECT officepea FROM `users` WHERE `empcode` = '$empcode'";
$result2 = mysqli_query($connection, $sql2);
if (mysqli_num_rows($result2) > 0) {
    // output data of each row
    while ($row = mysqli_fetch_assoc($result2)) {
        $officepea = $row["officepea"];
    }

} else {
    echo json_encode(array('status' => '3', 'message' => 'Record add successfully'));

    exit;

}

$sql = "INSERT INTO `jobmeter` (`id`, `workorder`, `namecustomer`, `address`, `tel`,`pea_new`,`pea_old`, `lat`, `lon`, `officepea`, `date_order`) VALUES (NULL, '$textWorkorder', '$textNamecustomer', '$textAddress', '$textTel ','$textPeanew ','$textPeaold ', '$textLat', '$textLon', '$officepea ', '$dateok');";

$query = mysqli_query($connection, $sql);

if ($query) {
    echo json_encode(array('status' => '1', 'message' => ' สำเร็จ'));

} else {
    echo json_encode(array('status' => '2', 'message' => 'ไม่สำเร็จโปรดติดต่อ ADMIN'));
}

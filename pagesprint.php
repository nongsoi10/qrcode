<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="./add/jquery.datetimepicker.css" />
    <title>ระบบแสดง QRCODE</title>
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">ระบบบันทึกข้อมูลมิเตอร์</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ตัวจัดการต่างๆ
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="./pagesinsert.php">เพิ่มข้อมูล</span></a>
                            <a class="dropdown-item" href="./pageslist.php">ข้อมูลทั้งหมด/แก้ไข</a>
                            <a class="dropdown-item" href="./pagesprint.php">ปริ้นข้อมูล</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <hr>
        <div class="form-group row">
            <label for="datetimepicker" class="col-sm-1 col-form-label">เลือกวันที่</label>
            <div class="col-sm-3">
                <input class="form-control mx-auto" id="datetimepicker" type="text">
            </div>
            <button type="button" class="btn btn-success ml-0" id="btnShow">Show</button>
        </div>
        <hr>
        <div class="card-columns" id="resultcard">

        </div>
    </div>
    </div>
</body>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
<script src="./add/jquery.datetimepicker.full.js"></script>
<script type="text/javascript" src="./add/jquery.qrcode.min.js"></script>

<script type="text/javascript">
jQuery('#datetimepicker').datetimepicker({
    timepicker: false,
    format: 'Y-m-d'
});

jQuery('#qrgps') . qrcode("this plugin is great");


$(document).ready(function() {

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    $('#datetimepicker').val(today);

    $("#btnShow").click(function() {
        var dateClick = $('#datetimepicker').val();
        $.ajax({
            url: "./php/print.php",
            method: "POST",
            data: {
                dateClick: dateClick
            },
            dataType: "html",
            success: function(data) {                           
                $('#resultcard').html(data);
            }
        });
    });

});
</script>

</html>
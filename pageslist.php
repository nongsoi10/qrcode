<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Datatable CSS -->
    <link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
    <title>ระบบแสดง QRCODE</title>
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">ระบบบันทึกข้อมูลมิเตอร์</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ตัวจัดการต่างๆ
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="./pagesinsert.php">เพิ่มข้อมูล</span></a>
                            <a class="dropdown-item" href="./pageslist.php">ข้อมูลทั้งหมด/แก้ไข</a>
                            <a class="dropdown-item" href="./pagesprint.php">ปริ้นข้อมูล</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <hr>
        <table id='empTable' class='display dataTable'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>หมายเลขใบงาน</th>
                    <th>ผู้ใช้ไฟ</th>
                    <th>ที่อยู่</th>
                    <th>เบอร์โทร</th>
                    <th>PEAติดตั้ง</th>
                    <th>PEAสับเปลี่ยน</th>
                    <th>LAT</th>
                    <th>LONG</th>
                    <th>การไฟฟ้า</th>
                    <th>EDIT</th>
                    <th>DELETE</th>
                </tr>
            </thead>
        </table>
        <div id="add_data_Modal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">แก้ไขข้อมูล</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form method="post" id="insert_form">
                            <label>หมายเลขคำร้อง</label>
                            <input type="text" name="workorder" id="workorder" class="form-control" />
                            <label>ผู้ใช้ไฟ</label>
                            <input type="text" name="namecustomer" id="namecustomer" class="form-control" />
                            <label>ที่อยู่</label>
                            <input type="text" name="address" id="address" class="form-control" />
                            <label>เบอร์โทร</label>
                            <input type="text" name="tel" id="tel" class="form-control" />
                            <label>PEA ติดตั้ง</label>
                            <input type="text" name="pea_new" id="pea_new" class="form-control" />
                            <label>PEA สับเปลี่ยน</label>
                            <input type="text" name="pea_old" id="pea_old" class="form-control" />
                            <label>Lat</label>
                            <input type="text" name="lat" id="lat" class="form-control" />
                            <label>Long</label>
                            <input type="text" name="lon" id="lon" class="form-control" />
                            <input type="hidden" name="idup" id="idup" />
                            <hr>
                            <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#empTable').DataTable({
        "order": [
            [0, 'DESC']
        ],
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post',
        'ajax': {
            'url': './php/list.php'
        },
        "language": {
            "lengthMenu": "จำนวน _MENU_ แถว",
            "zeroRecords": "Nothing found - sorry",
            "info": "หน้า page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        },
        'columns': [{
            data: 'id'
        }, {
            data: 'workorder'
        }, {
            data: 'namecustomer'
        }, {
            data: 'address'
        }, {
            data: 'tel'
        }, {
            data: 'pea_new'
        }, {
            data: 'pea_old'
        }, {
            data: 'lat'
        }, {
            data: 'lon'
        }, {
            data: 'officepea'
        }, {
            data: 'edit'
        }, {
            data: 'del'
        }, ]
    });
    $('#empTable tbody').on('click', '#btEdit', function() {
        //alert($(this).attr("value"));

        var id = $(this).attr("value");
        //
        $.ajax({
            url: "./php/fetch.php",
            method: "POST",
            data: {
                id: id
            },
            dataType: "json",
            success: function(data) {
                $('#idup').val(data.id);
                $('#workorder').val(data.workorder);
                $('#namecustomer').val(data.namecustomer);
                $('#address').val(data.address);
                $('#tel').val(data.tel);
                $('#pea_new').val(data.pea_new);
                $('#pea_old').val(data.pea_old);
                $('#lat').val(data.lat);
                $('#lon').val(data.lon);
                $('#insert').val("Update");
                $("#add_data_Modal").modal("show");
            }
        });
    });

    $('#insert_form').on("submit", function(event) {
        event.preventDefault();

        $.ajax({
            url: "./php/update.php",
            method: "POST",
            data: $('#insert_form').serialize(),
            beforeSend: function() {
                $('#insert').val("Inserting");
            },
            success: function(result) {
                $('#insert_form')[0].reset();
                $('#add_data_Modal').modal('hide');
                if (result.status == 1) // Success
                {
                    alert(result.message);
                    location . reload();

                } else // Err
                {
                    alert(result.message);                    
                }
            }
        });

    });
    $('#empTable tbody').on('click', '#btDel', function() {
        //alert($(this).attr("value"));

        var id = $(this).attr("value");
        //
        $.ajax({
            type: "POST",
            url: "./php/delete.php",
            data: {
                id: id
            },
            dataType: "json",
            success: function (result) {
                if (result.status == 1) // Success
                {
                    alert(result.message);
                    location . reload();

                }
                else // Err
                {
                    alert(result.message);
                }
            }
        });
    });
});
</script>

<script type="text/javascript">

</script>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>ระบบแสดงQRCODE</title>
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">ระบบบันทึกข้อมูลมิเตอร์</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ตัวจัดการต่างๆ
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="./pagesinsert.php">เพิ่มข้อมูล</span></a>
                            <a class="dropdown-item" href="./pageslist.php">ข้อมูลทั้งหมด/แก้ไข</a>
                            <a class="dropdown-item" href="./pagesprint.php">ปริ้นข้อมูล</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <hr>
        <div class="justify-content-center">
            <div class="card">
                <h5 class="card-header">บันทึกข้อมูลมิเตอร์</h5>
                <div class="card-body">
                    <div class="form-group">
                        <label for="textWorkorder">เลขที่ใบสั่งงาน</label>
                        <input type="text" class="form-control" id="textWorkorder">
                        <!-- <input type="text" class="form-control" id="textEmp" aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-muted">ต้องการใช้งานระบบโปรดติดต่อ
                            Admin.</small> -->
                    </div>
                    <div class="form-group">
                        <label for="textNamecustomer">ผู้ใช้ไฟ</label>
                        <input type="text" class="form-control" id="textNamecustomer">
                    </div>
                    <div class="form-group">
                        <label for="textAddress">ที่อยู่</label>
                        <input type="text" class="form-control" id="textAddress">
                    </div>
                    <div class="form-group">
                        <label for="textTel">เบอร์โทร</label>
                        <input type="text" class="form-control" id="textTel">
                    </div>
                        <div class="form-group">
                        <label for="textPeanew">PEA ติดตั้งใหม่</label>
                        <input type="text" class="form-control" id="textPeanew">
                    </div>
                        <div class="form-group">
                        <label for="textPeaold">PEA เก่าสับเปลี่ยน</label>
                        <input type="text" class="form-control" id="textPeaold">
                    </div>
                    <div class="form-group">
                        <label for="textLat">Latitude</label>
                        <input type="text" class="form-control" id="textLat">
                    </div>
                    <div class="form-group">
                        <label for="textLon">Longitude</label>
                        <input type="text" class="form-control" id="textLon">
                    </div>
                    <div class="mt-2">
                        <button type="submit" class="btn btn-success" id="btnSend">SAVE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>   
 </div> 
</body>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.0.js" integrity="sha256-r/AaFHrszJtwpe+tHyNi/XCfMxYpbsRg2Uqn0x3s2zc="
    crossorigin="anonymous"></script>

<script type="text/javascript">

    $("#btnSend").click(function () {

        var textWorkorder = $("#textWorkorder").val();
        var textNamecustomer = $("#textNamecustomer").val();
        var textAddress = $("#textAddress").val();
        var textTel = $("#textTel").val();
        var textPeanew = $("#textPeanew").val();
        var textPeaold = $("#textPeaold").val();
        var textLat = $("#textLat").val();
        var textLon = $("#textLon").val();

        $.ajax({
            type: "POST",
            url: "./php/insert.php",
            data: {
                textWorkorder: textWorkorder,
                textNamecustomer: textNamecustomer,
                textAddress: textAddress,
                textTel: textTel,
                textPeanew: textPeanew,
                textPeaold: textPeaold,
                textLat: textLat,
                textLon: textLon
            },
            dataType: "json",
            success: function (result) {
                if (result.status == 1) // Success
                {
                    alert(result.message);
                    $("#textWorkorder").val('');
                    $("#textNamecustomer").val('');
                    $("#textAddress").val('');
                    $("#textTel").val('');
                    $("#textPeanew").val('');
                    $("#textPeaold").val('');
                    $("#textLat").val('');
                    $("#textLon").val('');

                }
                else // Err
                {
                    alert(result.message);
                }
            }
        });

    });

</script>

</html>

<?php
$db['db_host'] = "localhost";
$db['db_user'] = "root";
$db['db_pass'] = "root";
$db['db_name'] = "qrcode";

foreach ($db as $key => $value) {

    define(strtoupper($key), $value);
}

$connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
mysqli_query($connection, "SET CHARACTER SET 'utf8'");
mysqli_query($connection, "SET SESSION collation_connection ='utf8_unicode_ci'");
/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

/* close connection */
//mysqli_close($connection);
